const { Builder, By, Key, until } = require('selenium-webdriver');
const chrome = require('selenium-webdriver/chrome');
const chromedriver = require('chromedriver');

chrome.setDefaultService(new chrome.ServiceBuilder(chromedriver.path).build());
(async function example() {
  let driver = new Builder().forBrowser('chrome').build();
  try {
    // Navigate to Url
    await driver.get('https://www.google.com');

    // Enter text "cheese" and perform keyboard action "Enter"
    await driver.findElement(By.name('q')).sendKeys('cheese', Key.ENTER);

    let firstResult = await driver.wait(
      until.elementLocated(By.css('h3>div')),
      10000
    );

    console.log(await firstResult.getAttribute('textContent'));
  } finally {
    // driver.quit();
    console.log('selesai');
  }
})();
